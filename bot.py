import asyncio
import sqlite3

from aiogram import Bot, types
from aiogram.dispatcher import Dispatcher
from aiogram.types import ReplyKeyboardMarkup, KeyboardButton, \
    InlineKeyboardMarkup, InlineKeyboardButton
from aiogram.utils import executor

chann = 'https://t.me/TGPingCli'

mn1 = InlineKeyboardButton('🔗 Получить прокси', callback_data='getproxy')
mn2 = InlineKeyboardButton('🔖 Больше прокси', url=chann)
keym1 = InlineKeyboardMarkup().row(mn1).add(mn2)

mn3 = InlineKeyboardButton('🎡 Канал с прокси', url=chann)
mn4 = InlineKeyboardButton('🎈 Обновить', callback_data='refresh')
keym2 = InlineKeyboardMarkup().add(mn3)

TOKEN = '<TOKEN>'

bot = Bot(token=TOKEN)
dp = Dispatcher(bot)

print('START')

@dp.message_handler(commands=['start'])
async def start(msg: types.Message):
    msid1 = None
    chtid = msg.chat.id
    # Проверяем пользователя на наличие в базе
    conn = sqlite3.connect("bd.db")  # Получаем инфу о человеке
    cursor = conn.cursor()
    cursor.execute('SELECT [id]'
                   ',[name]'
                   ',[lang]'
                   'FROM [users]'
                   f'WHERE id={chtid}')
    info = cursor.fetchall()
    conn.close()

    if info:
        # Человек есть - просто посылаем в меню
        await menu(msg, chtid)
    else:  # Человека нет - значит будем добавлять его
        try:
            language = 'ru'
            conn = sqlite3.connect("bd.db")  # Добавляем инфу в базу
            cursor = conn.cursor()
            cursor.execute(f'''
                            INSERT INTO [users]
                                    ([id]
                                    ,[name]
                                    ,[lang])
                            VALUES
                                    ({msg.chat.id}
                                    ,'{msg.chat.first_name}'
                                    ,'{language}')
                            ''')
            conn.commit()
            await menu(msg, chtid)
        except Exception as err:
            await bot.send_message(chtid, 'Произошла ошибка.')


async def menu(msg, chtid):
    msid = msg.message_id + 1
    await bot.send_message(chat_id=chtid, text="🎾 *Привет, я могу предоставить бесплатные прокси!*\n😎 Моим плюсом "
                                               "является то, что прокси обновляются каждый час и если *не работает "
                                               "один - заработает другой*.\n\n*Жми на кнопку* и ты получишь 5 свежих "
                                               "прокси, а если тебе этого мало, переходи на мой канал - там их очень "
                                               "много 😈", reply_markup=keym1, parse_mode='markdown')

    @dp.callback_query_handler(func=lambda c: c.data == 'getproxy')
    async def getproxy(c: types.CallbackQuery):
        await bot.answer_callback_query(c.id)
        chtid = c.from_user.id
        msid = c.message.message_id

        conn = sqlite3.connect("proxies.db")
        cursor = conn.cursor()
        sql= ('SELECT [country]\n'
              '  ,[ping]\n'
              '  ,[type]\n'
              '  ,[ip]\n'
              'FROM [proxies] limit 5')
        cursor.execute(sql)
        prxlist = cursor.fetchall()
        prx1 = f'🎁 *Лови 5 сочных прокси:*\n' \
               f'1. Страна: *{prxlist[0][0]}* | Пинг: *{prxlist[0][1]}* | Адрес: {prxlist[0][3]}\n' \
               f'2. Страна: *{prxlist[1][0]}* | Пинг: *{prxlist[1][1]}* | Адрес: {prxlist[1][3]}\n' \
               f'3. Страна: *{prxlist[2][0]}* | Пинг: *{prxlist[2][1]}* | Адрес: {prxlist[2][3]}\n' \
               f'4. Страна: *{prxlist[3][0]}* | Пинг: *{prxlist[3][1]}* | Адрес: {prxlist[3][3]}\n' \
               f'5. Страна: *{prxlist[4][0]}* | Пинг: *{prxlist[4][1]}* | Адрес: {prxlist[4][3]}\n\n' \
               f'🍉 *Напомню:* если желаешь больше прокси переходи на канал:'
        await bot.edit_message_text(text=prx1, message_id=msid, chat_id=chtid, parse_mode='Markdown',
                                    reply_markup=keym2)


if __name__ == '__main__':
    executor.start_polling(dp)
