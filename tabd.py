import time
import asyncio
from proxybroker import Broker
import sqlite3
from aiogram import Bot, types
from aiogram.dispatcher import Dispatcher
from aiogram.utils import executor
TOKEN = '<TOKEN>'

bot = Bot(token=TOKEN)
channel = -1001298345698


async def show(proxies):
    while True:
        print('NEW')
        while True:
            proxy = await proxies.get()
            if proxy is None: break
            prx = str(proxy)
            prxl = prx.replace('<Proxy ', '')
            prxl = prxl.replace(': ', '/')
            prxl = prxl.replace('>', '')
            prxlist = prxl.split(' ')
            conn = sqlite3.connect("proxies.db")  # Добавляем инфу в базу
            cursor = conn.cursor()
            cursor.execute(f'''
                    INSERT INTO [proxies]
                ([country]
                ,[ping]
                ,[type]
                ,[ip])
            VALUES
                ('{prxlist[0]}'
                ,'{prxlist[1]}'
                ,'{prxlist[2]}'
                ,'{prxlist[3]}')
                            ''')
            conn.commit()
            await bot.send_message(channel, f'*Прокси:* {prxlist[3]}\n'
                                            f'*Страна:* {prxlist[0]}', parse_mode='Markdown')
            time.sleep(3)
        time.sleep(3600)


proxies = asyncio.Queue()
broker = Broker(proxies)
tasks = asyncio.gather(
    broker.find(types=['HTTP', 'HTTPS'], limit=10),
    show(proxies))

loop = asyncio.get_event_loop()
loop.run_until_complete(tasks)
